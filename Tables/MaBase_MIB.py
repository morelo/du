from dataclasses import dataclass

@dataclass(unsafe_hash=True)
class Cabine:
	NoCabine: str
	NoAllee: str

@dataclass(unsafe_hash=True)
class Gardien:
	NomAgent: str
	NoCabine: str

@dataclass(unsafe_hash=True)
class Responsable:
	NoAllee: str
	NomAgent: str

@dataclass(unsafe_hash=True)
class Miam:
	NomAlien: str
	Aliment: str

@dataclass(unsafe_hash=True)
class Ville:
	Ville: str
	Planete: str

@dataclass(unsafe_hash=True)
class Alien:
	NomAlien: str
	Sexe: str
	Planete: str
	NoCabine: str

@dataclass(unsafe_hash=True)
class Agent:
	NomAgent: str
	VilleAgent: str

BaseCabines = { Cabine('1', '1'), Cabine('2', '1'), Cabine('3', '1'), Cabine('4', '1'), Cabine('5', '1'), Cabine('6', '2'), Cabine('7', '2'), Cabine('8', '2'), Cabine('9', '2') }

BaseGardiens = { Gardien('Branno', '1'), Gardien('Darell', '2'), Gardien('Demerzel', '3'), Gardien('Seldon', '4'), Gardien('Dornick', '5'), Gardien('Hardin', '6'), Gardien('Trevize', '7'), Gardien('Pelorat', '8'), Gardien('Riose', '9') }

BaseResponsables = { Responsable('1', 'Seldon'), Responsable('2', 'Pelorat') }

BaseMiams = { Miam('Zorglub', 'Bortsch'), Miam('Blorx', 'Bortsch'), Miam('Urxiz', 'Zoumise'), Miam('Zbleurdite', 'Bortsch'), Miam('Darneurane', 'Schwanstucke'), Miam('Mulzo', 'Kashpir'), Miam('Zzzzzz', 'Kashpir'), Miam('Arghh', 'Zoumise'), Miam('Joranum', 'Bortsch') }

BaseVilles = { Ville('Terminus', 'Trantor'), Ville('Arcturus', 'Euterpe'), Ville('Kalgan', 'Helicon'), Ville('Hesperos', 'Euterpe'), Ville('Siwenna', 'Gaia') }

BaseAliens = { Alien('Zorglub', 'M', 'Trantor', '1'), Alien('Blorx', 'M', 'Euterpe', '2'), Alien('Urxiz', 'M', 'Aurora', '3'), Alien('Zbleurdite', 'F', 'Trantor', '4'), Alien('Darneurane', 'M', 'Trantor', '5'), Alien('Mulzo', 'M', 'Helicon', '6'), Alien('Zzzzzz', 'F', 'Aurora', '7'), Alien('Arghh', 'M', 'Nexon', '8'), Alien('Joranum', 'F', 'Euterpe', '9') }

BaseAgents = { Agent('Branno', 'Terminus'), Agent('Darell', 'Terminus'), Agent('Demerzel', 'Arcturus'), Agent('Seldon', 'Terminus'), Agent('Dornick', 'Kalgan'), Agent('Hardin', 'Terminus'), Agent('Trevize', 'Hesperos'), Agent('Pelorat', 'Kalgan'), Agent('Riose', 'Terminus'), Agent('Palver', 'Siwenna'), Agent('Amaryl', 'Arcturus') }

