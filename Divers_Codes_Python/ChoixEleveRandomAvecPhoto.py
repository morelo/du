#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  8 13:25:28 2018

@author: moi
"""
import random, sys
import tkinter as tk
from PIL import ImageTk, Image
from functools import reduce


## le fichier contenant les éleves une colonne NOM, une autre prénom
fichier = "/home/moi/LYCEE/TS/2018_19/Classe_TERMINALE_S3.csv"
fic = open(fichier, 'r')
lu = fic.readlines()
TS3 = []
for ligne in lu:
    li = ligne[:-2].strip().split(',')
    nfi = reduce(lambda x, y: x + y, li[:-1], '')
    TS3.append([li[-1] + ' ' + nfi, (nfi+li[-1][0]).lower()+'.jpg'])
# les photos sont nommés nom et initiale du prénom > jpg : connang.jpg

victime, photo = random.choice(TS3)

lab = '\n\n  And the winner is...\n' + str(victime)
window = tk.Tk()
window.title("Choisissons un élève de TS3")
window.configure(background='black')

path="/home/moi/LYCEE/TS/2018_19/pics/" + photo
img = ImageTk.PhotoImage(Image.open(path))


tk.Label(window, image=img, text=lab, compound=tk.BOTTOM, font=("Arial Bold", 30), foreground='white', background='black').pack()


window.bind("<Escape>",lambda event: window.destroy())
window.mainloop()
