# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 12:52:11 2019

@author: gconnan
"""
from math import sqrt
from dataclasses import dataclass


@dataclass
class Vecteur:
    x: float
    y: float


@dataclass
class Point:
    x: float
    y: float


Equation = str


#  CALCUL VECTORIEL
# Un vecteur sera représenté  par un couple (abs, ord)
# Par exemple le vecteur de coordonnées 10 et 20 sera
# v = Vecteur(10, 20)
# On accède à son abscisse (son premier élément) par :
# v.x
# Son ordonnée sera obtenue en appelant son 2e élément
# v.y


v = Vecteur(10, 20)
print("Le vecteur a pour coordonnées :" + str(v))
print("Son abscisse est " + str(v.x))
print("Son ordonnéee est " + str(v.y))

# Créez une fonction qui calcule la norme
# d'un vecteur


def norme(v: Vecteur) -> float:
    """
    Calcule la norme d'un vecteur v
    Par exemple :

    >>> norme(Vecteur(3,4))
    5.0

    En effet, ||v|| = Racine(absisse de v au carré + ordo de v au carré)
    """
    return sqrt(v.x**2 + v.y**2)

# Créez une fonction qui prend deux points
#  A et B  définis par leurs coordonnéées en argument
#  et renvoie les coordonnées du vecteur AB


def vecteur(A: Point, B: Point) -> Vecteur:
    """
    Calcule les coordonnéées du vecteur AB si l'on
    connaît les points A et B

    >>> vecteur(Point(4,5), Point(7,9))
    Vecteur(x=3, y=4)
    """
    return Vecteur(B.x - A.x,  B.y - A.y)


# Créez une fonction qui prend deux vecteurs en arguments
# et qui renvoie True si les vecteurs sont colinéaires et
# False sinon

def sont_colineaires(v1: Vecteur, v2: Vecteur) -> bool:
    """
    Produits en croix égaux

    >>> sont_colineaires(Vecteur(1, 2), Vecteur(10, 20))
    True
    """
    return v1.x * v2.y == v1.y * v2.x

# Créez une fonction qui renvoie le coefficient directeur
# d'une droite définie par deux points


def coeff_dir(A: Point, B: Point) -> float:
    """
    Calcule le coefficient directeur de (AB) sous forme de flottant
    
    >>> coeff_dir(Point(1,3), Point(2,5))
    2.0
    
    """
    dep_vert = B.y - A.y
    dep_hori = B.x - A.x
    assert dep_hori != 0, "La droite (%s, %s) est verticale" % (str(A), str(B))
    return dep_vert / dep_hori

# Créez une fonction qui vérifie si 3 points sont alignés


def alignes(A: Point, B: Point, C: Point) -> bool:
    """
    A, B, C sont alignés ssi les vecteurs AB et AC sont colinéaires

    >>> A, B, C = Point(1, 3), Point(2,5), Point(3,7)
    >>> alignes(A, B, C)
    True
    """
    AB = vecteur(A, B)
    AC = vecteur(A, C)
    return sont_colineaires(AB, AC)

# Créez une fonction qui renvoie le vecteur directeur de la droite
# (AB) d'abscisse 1 s'il existe


def vec_dir_normalisé(A: Point, B: Point) -> Vecteur:
    """
    >>> vec_dir_normalisé(Point(1,3), Point(2,5))
    Vecteur(x=1, y=2.0)
    """
    return Vecteur(1, coeff_dir(A, B))

# Créez une fonction qui renvoie une équation cartésienne de
# la droite (AB) sous la forme d'une chaîne de
# caractères


def cartésienne(A: Point, B: Point) -> Equation:
    """
    On renvoie une chaîne de caractères représentant
    ax + by + c = 0

    >>> A, B = Point(1, 3), Point(2,5)
    >>> cartésienne(A, B)
    '2.00*x + (-1.00*y) + (1.00) == 0'
    """
    a = B.y - A.y
    b = A.x - B.x
    c = -A.x*(B.y - A.y) + A.y*(B.x - A.x)
    return "%.2f*x + (%.2f*y) + (%.2f) == 0" % (a, b, c)


# Créez une fonction qui teste si un point appartient à une droite dont
# on connaît une équation cartésienne.

def verifie_eq(A: Point, eq: Equation) -> bool:
    """
    >>> A, B = Point(1, 3), Point(2,5)
    >>> eq = cartésienne(A, B)
    >>> verifie_eq(A, eq)
    True
    """
    x, y = A.x, A.y
    return eval(eq)



if __name__ == "__main__":
    import doctest
    doctest.testmod()