#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 16:31:13 2019

@author: moi
"""

from liste_implem_list import *
from copy import copy
from typing import Callable, TypeVar

Sommable = TypeVar('Sommable', int, float, str)

def somme(xs: Liste[Sommable], neutre: Sommable = 0) -> Sommable:
    """
    Calcule la somme des éléments d'une liste,
    en précisant éventuellement le neutre de la somme

    >>> liste_test = liste(3,liste(2,liste(1,Vide)))
    >>> somme(liste_test)
    6
    >>> somme(Vide)
    0
    >>> liste_car = liste('p',liste('a', liste('p', liste('a',Vide))))
    >>> somme(liste_car, '')
    'papa'
    """
    if est_vide(xs):
        return neutre
    else:
        return tete(xs) + somme(queue(xs), neutre)


def retourne(xs: Liste[T]) -> Liste[T]:
    """
    Renvoie la liste dans l'autre sens

    >>> liste_test = liste(3,liste(2,liste(1,Vide)))
    >>> retourne(liste_test)
    [1, 2, 3]
    >>> liste_test
    [3, 2, 1]
    >>> retourne(Vide)
    []
    """
    sx = Vide
    copie = xs
    while not est_vide(copie):
        sx = insere(tete(copie),sx)
        copie = queue(copie)
    return sx

def filtre(xs: Liste[T], pred: Callable[[T], bool]) -> Liste[T]:
    """
    Renvoie la liste des éléments de xs qui satisfont le prédicat

    >>> liste_test = liste(3,liste(2,liste(1,Vide)))
    >>> filtre(liste_test, lambda x: x%2 == 0)
    [2]
    >>> filtre(liste_test, lambda x: x%2 == 1)
    [1, 3]
    """
    xf: Liste[T] = Vide
    copie: Liste[T] = xs
    while not est_vide(copie):
        x: T = tete(copie)
        if pred(x):
            xf = insere(x, xf)
        copie = queue(copie)
    return xf



#########################################################
#
#
#          PYTHON STYLE
#
#
#########################################################

def filtrep(xs: List[T], pred: Callable[[T], bool]) -> List[T]:
    return [x for x in xs if pred(x)]

def retournep(xs: List[T]) -> List[T]:
    return xs[::-1]





if __name__ == "__main__":
    import doctest
    doctest.testmod()
