import numpy as np
from copy import copy, deepcopy

# a = np.array([[1,2,3],[4,5,6]])

# b = (np.array(np.arange(1,7))).reshape(2,3)

# c = np.matrix([[1,2,3],[4,5,6]])

# d = np.matrix('1 2 3; 4 5 6')


def trouve_pivot(A,dep) :
    ind_max = dep
    for k in range(dep + 1, len(A)) :
        if abs(A[k,dep]) > abs(A[ind_max,dep]) :
            ind_max = k
    return ind_max

def transvection(A,i,j,k) :
    A[i] += k * A[j]

def echange(A,i,j) :
    tmp = copy(A[i])
    A[i], A[j] = A[j], tmp

def gauss(A,B) :
    T = np.concatenate((A,B), axis = 1)
    ind_max = len(A)
    assert ind_max == len(A[0]), "Matrice non carrée"
    for row_piv in range(ind_max) :
        ligne_piv = trouve_pivot(T,row_piv)
        if ligne_piv > row_piv :
            echange(T, row_piv, ligne_piv)
        pivot = T[row_piv, row_piv]
        for sub_row in range(row_piv + 1, ind_max):
            coeff = T[sub_row, row_piv] / pivot
            transvection(T, sub_row, row_piv, -coeff)
    Sol = np.arange(ind_max, dtype=np.float)
    for i in range(ind_max-1, -1, -1):
        Sol[i] = (T[i,ind_max] - sum(T[i,j]*Sol[j] for j in range(i+1, ind_max))) / T[i,i]
    return Sol

def det(A) :
    T = copy(A)
    ind_max = len(A)
    assert ind_max == len(A[0]), "Matrice non carrée"
    d = 1
    for row_piv in range(ind_max) :
        ligne_piv = trouve_pivot(T,row_piv)
        if ligne_piv > row_piv :
            echange(T, row_piv, ligne_piv)
            d *= -1
        pivot = T[row_piv, row_piv]
        d *= pivot
        for sub_row in range(row_piv + 1, ind_max):
            coeff = T[sub_row, row_piv] / pivot
            transvection(T, sub_row, row_piv, -coeff)
    return d
    
