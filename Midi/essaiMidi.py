from pyknon.genmidi import Midi
from pyknon.music import NoteSeq

riff = "B4, B4, R8 B8, C#8' D4' R8 D8' D8' C#8' C#4' "
riffs = NoteSeq(riff*2 + riff[0:3])
midi = Midi(1, tempo=130, instrument=29)
midi.seq_notes(riffs, track=0)
midi.write("demo.midi")




